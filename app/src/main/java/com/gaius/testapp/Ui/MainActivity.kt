package com.gaius.testapp.Ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.gaius.testapp.Adapters.PublicPostsAdapter
import com.gaius.testapp.DB.DatabaseHelper
import com.gaius.testapp.DB.DbModel
import com.gaius.testapp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var db: DatabaseHelper? =null

    var postList=ArrayList<DbModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db=DatabaseHelper(applicationContext)
        //returns array list of dbModel
        postList=db!!.getDbData()
        initViews()
    }

    override fun onResume() {
        super.onResume()
        postList=db!!.getDbData()
        public_posts_rv.adapter=PublicPostsAdapter(this,postList)
    }


    private fun initViews() {

        public_posts_rv.layoutManager=LinearLayoutManager(applicationContext,LinearLayoutManager.VERTICAL,false)
        public_posts_rv.adapter=PublicPostsAdapter(applicationContext,postList)
        addNewPost.setOnClickListener {
            startActivity(Intent(this@MainActivity,PostingActivity::class.java))
        }
    }
}
