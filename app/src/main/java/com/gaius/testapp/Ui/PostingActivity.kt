package com.gaius.testapp.Ui

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.gaius.testapp.DB.DatabaseHelper
import com.gaius.testapp.DB.DbModel
import com.gaius.testapp.R
import com.gaius.testapp.Utils.DbBitmapUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_posting.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class PostingActivity : AppCompatActivity() {

    var isImageSelected:Boolean=false
    private val SELECT_PICTURE=201
    var db: DatabaseHelper?=null
    var bitsArray:ByteArray?=null
    //hardcoded array of content type
    private val types = arrayOf(
        "Animal",
        "Bird",
        "Flower",
        "city",
        "Car",
        "Sports",
        "Landscape",
        "Girl",
        "Boy",
        "River",
        "Add Your Own")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posting)
        db=DatabaseHelper(applicationContext)
        initOnClicks()
    }

    private fun initOnClicks() {
        postImage.setOnClickListener {

            //if image is not selected  intent to select a image
            if (!isImageSelected) {
                selectImage()
            }

            //if image is already selected ask the user whether he want to change image

            else{
                AlertDialog.Builder(this)
                    .setMessage("Reselect Image?")
                    .setPositiveButton("YES")
                    { _, _ ->
                        //re selecting image
                        selectImage() }
                    .setNegativeButton("No")
                    { dialog, _ ->
                        //cancel dialog
                      dialog.cancel()
                    }
                    .show()
            }
        }
        //PopUp To Select Content Type

        postContentType.setOnClickListener {

            val b = AlertDialog.Builder(this)
            b.setTitle("Select A Category")
            b.setItems(types) { d,position->
                if (types[position]=="Add Your Own")
                {
                    postContentType.hint="Add Your Own Type here"
                    postContentType.isFocusableInTouchMode = true
                }
                else{
                    postContentType.setText(types[position])
                }
            }

            b.show()
        }

        postButton.setOnClickListener {

            //Check Whether any field is empty

            if (isImageSelected&& postContentType.text.toString() != ""&&postDes.text.toString()!="") {

                //get current date and time
                val currentdate = Calendar.getInstance().time
                val date = SimpleDateFormat("dd-MM-yy c", Locale.US).format(currentdate)

                //extrating strings from edittext
                val postdescription=postDes.text.toString()
                val postType=postContentType.text.toString()
                val hashtagsused=getHashtags(postdescription)

                val model = DbModel(postdescription, postType, bitsArray!!, date,hashtagsused )
                if (db!!.insertIntoDB(model) > 0) {

                    //Insertion Successful
                    Toast.makeText(this, "POST CREATED", Toast.LENGTH_SHORT).show()
                    this.finish()

                } else {

                    //Insertion Successful
                    Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
            }
            else{

                if (!isImageSelected){
                    Toast.makeText(this, "Please Select Any Image", Toast.LENGTH_SHORT).show()
                }else if(postContentType.text.toString() == ""){
                    Toast.makeText(this, "Content Type Should Be Selected", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "Provide Some Description", Toast.LENGTH_SHORT).show()
                }
            }

        }


    }

    private fun getHashtags(postdescription: String): String {

        val regexPattern = "(#\\w+)"
        val p = Pattern.compile(regexPattern)
        val m = p.matcher(postdescription)
        var hashtag=""
        while (m.find()) {
            hashtag += m.group(1)

            //for this project iam concatenating each tags in real app we can use tag array
        }

        return hashtag
    }

    //function to select image
    private fun selectImage() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, SELECT_PICTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                // this is the image selected by the user

                val imageUri = data.data
                isImageSelected=true
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)

                //executing in async task inorder to speedup the process
                //here conversion from bitmap to byte array occur
                AsyncTask.execute {
                    bitsArray= DbBitmapUtility().getBytes(bitmap)
                }
                Picasso.get().load(imageUri).into(postImage)
            }
        }
    }

}
