package com.gaius.testapp.Adapters

import android.app.Dialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaius.testapp.DB.DbModel
import com.gaius.testapp.R
import com.gaius.testapp.Utils.DbBitmapUtility
import kotlinx.android.synthetic.main.details_dialog.*
import kotlinx.android.synthetic.main.post_recycleview_item.view.*


class PublicPostsAdapter(
    val applicationContext: Context,
    val postList: ArrayList<DbModel>
) :RecyclerView.Adapter<PublicPostsAdapter.PostsViewHolder> (){
    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): PostsViewHolder {
        val view=LayoutInflater.from(viewGroup.context).inflate(R.layout.post_recycleview_item,viewGroup,false)
        return PostsViewHolder(view)

    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val model=postList[position]
        holder.postcontent.text=model.content
        holder.posttime.text=model.time
        val bitmap=DbBitmapUtility().getImage(model.filepath)
        holder.postimage.setImageBitmap(bitmap)
        holder.postimage.setOnClickListener {

            // a simple dialog to show content details
            val d=Dialog(applicationContext)
            d.setContentView(R.layout.details_dialog)
            d.contentType.text=model.contenttype
            d.tagsUsed.text=model.tags
            d.show()
        }



    }
    class PostsViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        val postimage=itemView.postimage
        val postcontent=itemView.postcontent
        val posttime=itemView.posttime

    }

}

