package com.gaius.testapp.DB

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.lang.Exception

class DatabaseHelper( ctx: Context):SQLiteOpenHelper(ctx, DATABASENAME,null, DATABASE_V) {
    companion object{
        var DATABASENAME="TESTAPP"
        var DATABASE_V=2
    }
    val TABLE_NAME="TESTAPPDB"
    val COLUMN_ID="id"
    val COLUMN_CONTENT="content"
    val COLUMN_CONTENT_TYPE="contenttype"
    val COLUMN_FILEPATH="filepath"
    val COLUMN_TIME="time"
    val COLUMN_TAGS="tags"

    override fun onCreate(db: SQLiteDatabase?) {

        val sqlTable= "CREATE TABLE $TABLE_NAME"+
                    "("+COLUMN_ID+" INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_CONTENT + " TEXT," +
                    COLUMN_CONTENT_TYPE + " TEXT," +
                    COLUMN_TIME + " TEXT," +
                    COLUMN_TAGS + " TEXT," +
                    COLUMN_FILEPATH + " BLOB" +
                ");"
        db!!.execSQL(sqlTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")

    }

    fun insertIntoDB(model: DbModel):Long
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_CONTENT, model.content)
        values.put(COLUMN_CONTENT_TYPE, model.contenttype)
        values.put(COLUMN_FILEPATH, model.filepath)
        values.put(COLUMN_TIME, model.time)
        values.put(COLUMN_TAGS, model.tags)
        val success=db.insert(TABLE_NAME, null, values)
        db.close()
        return success
    }
    fun getDbData():ArrayList<DbModel>{

        val allData=ArrayList<DbModel>()
        val selectQuery = "SELECT  * FROM $TABLE_NAME"
        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        try {
            if (cursor.moveToFirst()) {
                do {
                    val dbModel = DbModel(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getBlob(5),
                        cursor.getString(3),
                        cursor.getString(4)
                    )
                    allData.add(dbModel)
                } while (cursor.moveToNext())
            }
        }catch (e:Exception)
        {

        }
        cursor.close()
        db.close()
        return allData
    }


}