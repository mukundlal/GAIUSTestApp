package com.gaius.testapp.DB

class DbModel(
              val content:String,
              val contenttype:String,
              val filepath:ByteArray,
              val time:String,
              val tags:String)
